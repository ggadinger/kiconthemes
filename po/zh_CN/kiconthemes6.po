msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-06 02:03+0000\n"
"PO-Revision-Date: 2023-09-16 10:06\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/kiconthemes/kiconthemes6.pot\n"
"X-Crowdin-File-ID: 42927\n"

#: widgets/kiconbutton.cpp:75
#, kde-format
msgctxt "@info:tooltip"
msgid "Select Icon…"
msgstr "选择图标…"

#: widgets/kicondialog.cpp:239
msgid "All"
msgstr "全部"

#: widgets/kicondialog.cpp:240
msgid "Actions"
msgstr "操作"

#: widgets/kicondialog.cpp:241
msgid "Applications"
msgstr "程序"

#: widgets/kicondialog.cpp:242
msgid "Categories"
msgstr "分类"

#: widgets/kicondialog.cpp:243
msgid "Devices"
msgstr "设备"

#: widgets/kicondialog.cpp:244
msgid "Emblems"
msgstr "徽标"

#: widgets/kicondialog.cpp:245
msgid "Emotes"
msgstr "表情"

#: widgets/kicondialog.cpp:246
msgid "Mimetypes"
msgstr "MIME 类型"

#: widgets/kicondialog.cpp:247
msgid "Places"
msgstr "位置"

#: widgets/kicondialog.cpp:248
msgid "Status"
msgstr "状态"

#: widgets/kicondialog.cpp:272
#, kde-format
msgctxt "Other icons"
msgid "Other"
msgstr "其他"

#: widgets/kicondialog.cpp:319
#, kde-format
msgid "Browse…"
msgstr "浏览…"

#: widgets/kicondialog.cpp:521
#, kde-format
msgid "No icons matching the search"
msgstr "未找到匹配该搜索条件的图标"

#: widgets/kicondialog.cpp:523
#, kde-format
msgid "No icons in this category"
msgstr "此类别中没有图标"

#. i18n: ectx: property (windowTitle), widget (QWidget, IconDialog)
#: widgets/kicondialog.cpp:610 widgets/kicondialog.ui:14
#, kde-format
msgid "Select Icon"
msgstr "选择图标"

#: widgets/kicondialog.cpp:610
#, kde-format
msgid ""
"*.ico *.png *.xpm *.svg *.svgz|Icon Files (*.ico *.png *.xpm *.svg *.svgz)"
msgstr ""
"*.ico *.png *.xpm *.svg *.svgz|图标文件 (*.ico *.png *.xpm *.svg *.svgz)"

#. i18n: ectx: property (accessibleName), widget (QComboBox, contextCombo)
#: widgets/kicondialog.ui:24
#, kde-format
msgid "Icon category"
msgstr "图标类别"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, searchLine)
#: widgets/kicondialog.ui:44
#, kde-format
msgid "Search Icons..."
msgstr "搜索图标..."
